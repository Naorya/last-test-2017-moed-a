<?php 
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{

	public function actionOwnpost()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnPostRule;
		$auth->add($rule);
	}

}
