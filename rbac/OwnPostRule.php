<?php 
namespace app\rbac;
use yii\rbac\Rule;

use Yii; 

class OwnPostRule extends Rule
{
	public $name = 'ownPostRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) 
		{//check if the user loged in.
			return isset($params['Post']) ? $params['Post']->author == $user : false;
		}
		return false;
	}
}
