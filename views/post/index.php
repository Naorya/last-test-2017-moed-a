<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'body',
            [
                'attribute' => 'author',
                'label' => 'Author',
                'value' => function($model){
                    return $model->userAuthor->name;
                },                                                                                  
                'filter'=>Html::dropDownList('PostSearch[author]', $user, $users, ['class'=>'form-control']),
            ],
           [
                'attribute' => 'category',
                'label' => 'Category',
                'value' => function($model){
                    return $model->categoryItem->category_name;
                },                                                                                  
                'filter'=>Html::dropDownList('PostSearch[category]', $category, $categories, ['class'=>'form-control']),
            ],
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function($model){
                    return $model->statusItem->status_name;
                },
                'filter'=>Html::dropDownList('PostSearch[status]', $status, $statuses, ['class'=>'form-control']),
            ],
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
