<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\ActiveRecord;
/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            [ // the status name 
                'label' => $model->attributeLabels()['status'],
                'value' => $model->statusItem->status_name, 
            ],
            [ // the category name 
                'label' => $model->attributeLabels()['category'],
                'value' => $model->categoryItem->category_name, 
            ],
            [ // the author name of the lead
                'label' => $model->attributeLabels()['author'],
                'format' => 'html',
                'value' => Html::a($model->userAuthor->name, 
                    ['user/view', 'id' => $model->userAuthor->id]),  
            ],
            [ // post created by
                'label' => $model->attributeLabels()['created_by'],
                'value' => isset($model->createdBy->name) ? $model->createdBy->name : 'No one!',    
            ],
            [ // post created at
                'label' => $model->attributeLabels()['created_at'],
                'value' => date('d/m/Y H:i:s', $model->created_at)
            ],              
            [ // post updated by
                'label' => $model->attributeLabels()['updated_by'],
                'value' => isset($model->updateddBy->name) ? $model->updateddBy->name : 'No one!',  
            ],
            [ // post updated at
                'label' => $model->attributeLabels()['updated_at'],
                'value' => date('d/m/Y H:i:s', $model->updated_at)
            ],      
        ],
    ]) ?>

</div>
