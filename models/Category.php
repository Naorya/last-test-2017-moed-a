<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $category_name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
   
     public static function getCategories()
    {
        $allCategories = self::find()->all();
        $allCategoriesArray = ArrayHelper::
                    map($allCategories, 'id', 'category_name');
        return $allCategoriesArray;
    }   
    
public static function getCategoriesWithAllCategories()
    {
        $allCategories = self::getCategories();
        $allCategories[null] = 'All Categories';
        $allCategories = array_reverse ( $allCategories, true );
        return $allCategories;    
    }   


    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_name' => 'Category Name',
        ];
    }
}
